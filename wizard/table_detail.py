# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2018-Today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

from odoo import api, fields, models, _

class TableDetail(models.TransientModel):
    _name = "table.detail"


    start_date = fields.Date(string="From Date", required='1')
    end_date = fields.Date(string="To Date", required='1')
    top_tables = fields.Selection([
        ('by_guests', 'Guests'),
        ('by_amounts', 'Sales')
    ], string='According to', default = 'by_guests')
    no_of_tables = fields.Integer(string='Number of Tables to Display', default = '10')

    @api.multi
    def check_report(self):
        data = {}
        data['form'] = self.read(['start_date', 'end_date', 'top_tables', 'no_of_tables'])[0]
        return self._print_report(data)


    def _print_report(self, data):
        data['form'].update(self.read(['start_date', 'end_date', 'top_tables', 'no_of_tables'])[0])
        if data['form']['top_tables'] == 'by_guests':
            return self.env.ref('top_tables.action_report_tables').report_action(self, data=data, config=False)
        else:
            return self.env.ref('top_tables.action_report_tables_amount').report_action(self, data=data, config=False)






   


    
