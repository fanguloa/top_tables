# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2018-Today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError



class ReportTables(models.AbstractModel):
    _name = 'report.top_tables.report_tables'
    
    @api.model
    def get_report_values(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        table_records = {}
        sorted_table_records = []
        sales = self.env['pos.order'].search([('state','in',('sale','done')),('date_order','>=',docs.start_date),('date_order','<=',docs.end_date)])
           for s in sales:
             if sales.table_id:
               if sales.table_id not in table_records:
                   table_records.update({sales.table_id:0})
                   table_records[sales.table_id] += sales.customer_count

        for table_id, customer_count in sorted(table_records.items(), key=lambda kv: kv[1], reverse=True)[:docs.no_of_tables]:
            sorted_table_records.append({'name':table_id.name, 'guests': int(customer_count)})

        return {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            'tables': sorted_table_records
        }
