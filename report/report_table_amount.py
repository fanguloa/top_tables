# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2018-Today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError



class ReportTablesAmount(models.AbstractModel):
    _name = 'report.top_tables.report_tables_amount'
    
    @api.model
    def get_report_values(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        table_records = {}
        sorted_table_records = []
        sales = self.env['pos.order'].search([('state','in',('sale','done')),('date_order','>=',docs.start_date),('date_order','<=',docs.end_date)])
        for s in sales:
            orders = self.env['pos.order.line'].search([('order_id','=',s.id)])
            for order in orders:
                if order.order_id:
                    if order.order_id not in table_records:
                        table_records.update({order.order_id:0})
                    table_records[order.order_id] += order.price_subtotal_incl
                    table_records[order.order_id] += sales.table_id

        #for order_id, price_subtotal_incl in sorted(table_records.items(), key=lambda kv: kv[1], reverse=True)[:docs.no_of_tables]:
            #sorted_table_records.append({'name':product_id.name, 'amount': int(price_subtotal_incl), 'pricelist_id' : product_id.company_id.currency_id })

        for table_id, price_subtotal_incl in sorted(table_records.items(), key=lambda kv: kv[1], reverse=True)[:docs.no_of_tables]:
            sorted_table_records.append({'name':table_id.name, 'amount': int(price_subtotal_incl), 'pricelist_id' : product_id.company_id.currency_id })

        return {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            'tables': sorted_product_records
        }

